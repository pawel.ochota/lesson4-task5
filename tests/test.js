const { createButton, openAlert } = require('..');
const { toHaveAttribute, toBeInTheDocument } = require('@testing-library/jest-dom/matchers');

expect.extend({ toHaveAttribute, toBeInTheDocument });

beforeEach( () => {
  window.alert = jest.fn();
  document.body.innerHTML = '';
});

describe('createButton', () => {
  it('should returns a HTMLElement', async () => {
    const result = createButton();

    expect(result instanceof HTMLElement).toBeTruthy();
  });

  it('should returns an element with #super-button id attribute', async () => {
    const button = createButton();

    expect(button).toHaveAttribute('id', 'super-button');
  });

  it('should have a #super-button in <body> element', async () => {
    createButton();

    expect(document.body.querySelector('#super-button')).toBeInTheDocument();
  });

  it('should invoke openAlert function when #super-button is clicked', async () => {
    expect(window.alert).not.toHaveBeenCalled();
    createButton();
    document.body.querySelector('#super-button').click();

    expect(window.alert).toHaveBeenCalledWith('super');
  });
});

describe('openAlert', () => {
  it('should invoke window.alert with string "super"', async () => {
    expect(window.alert).not.toHaveBeenCalled();
    openAlert();

    expect(window.alert).toHaveBeenCalledWith('super');
  });
});
